# activemq-exclusiv-consumer

This small example is for testing the exclusive consumer feature of ActiveMQ.  
Two Producers in two different threads send each 70 messages to the queue ```TEST.FOO```.  
Two Consumer also start in two different threads and listen for messages on the queue ```TEST.FOO```. Both consumer specify the Destination option ```consumer.exclusive=true```.

ActiveMQ version used in this example: ```5.15.13```

## Build
This project uses maven for building the sources: ```mvn clean package```

## Execute
After building the sources simply start the JAR file via command line ```java -jar target/exclusive-consumer-1.0-SNAPSHOT-spring-boot.jar``` 

## Result
After startup one thread consumes all messages. After ***50 messages*** the thread stops actively and the other thread continues with the message consumption.  

```
...
Consumer: foo received message #45 -> Hello world! (13) From: Thread-0 : 337463278
Consumer: foo received message #46 -> Hello world! (31) From: Thread-1 : 1181982079
Consumer: foo received message #47 -> Hello world! (14) From: Thread-0 : 337463278
Consumer: foo received message #48 -> Hello world! (32) From: Thread-1 : 1181982079
Consumer: foo received message #49 -> Hello world! (15) From: Thread-0 : 337463278
Consumer: foo received message #50 -> Hello world! (33) From: Thread-1 : 1181982079
Consumer: bar received message #1 -> Hello world! (16) From: Thread-0 : 337463278
Consumer: bar received message #2 -> Hello world! (34) From: Thread-1 : 1181982079
Consumer: bar received message #3 -> Hello world! (17) From: Thread-0 : 337463278
Consumer: bar received message #4 -> Hello world! (35) From: Thread-1 : 1181982079
Consumer: bar received message #5 -> Hello world! (18) From: Thread-0 : 337463278
...
```